# Installation 

Python app: We use VSCode to run the app locally which displays hello world message on web.

Additionally we need docker, gitlab and digitalocean account/apps to complete this task.


### Setting Up Development Environment

Clone the repository

```
git clone https://gitlab.com/liltai/lab-2
cd lab-2
```
Install packages
```
pip install -r requirements.txt
```

Run node application
```
python3 app.py
```

### Build docker image
```
docker build -t lab2app .
```

Run docker image
```
docker run -p 80:5000 -d lab2app
```

### Accessing the Deployed Application:

Application link: http://178.62.197.126